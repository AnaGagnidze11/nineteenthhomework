package com.example.nineteenthhomework

import android.app.Application

class App: Application() {

    external fun ApiKey(): String

    companion object {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }

        lateinit var apiKey: String
    }

    override fun onCreate() {
        super.onCreate()
        apiKey = ApiKey()
    }
}