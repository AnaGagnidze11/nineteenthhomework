package com.example.nineteenthhomework.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

typealias Inflater<I> = (LayoutInflater, ViewGroup?, Boolean) -> I
abstract class BaseFragment<VB: ViewBinding, VM: ViewModel>(private val inflate: Inflater<VB>, private val viewModelClass: Class<VM>): Fragment() {
    private var _binding: VB? = null
    val binding get() = _binding!!

    protected val viewModel: VM by lazy{
        ViewModelProvider(this).get(viewModelClass)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null) {
            _binding = inflate.invoke(inflater, container, false)
            setUpFragment()
        }
        return binding.root
    }

    abstract fun setUpFragment()


    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}