package com.example.nineteenthhomework.fragments

import android.widget.Toast
import com.example.nineteenthhomework.databinding.FragmentSignUpBinding
import com.example.nineteenthhomework.models.User
import com.example.nineteenthhomework.network.ResultControl
import com.example.nineteenthhomework.viewmodels.SignUpViewModel

class SignUpFragment : BaseFragment<FragmentSignUpBinding, SignUpViewModel>(FragmentSignUpBinding::inflate, SignUpViewModel::class.java) {

    override fun setUpFragment() {
        init()
    }

    private fun init(){
        binding.btnSignUp.setOnClickListener {
            signUp()
            observeSigningUp()
        }

        binding.btnSignIn.setOnClickListener {
            signIn()
            observeSigningIn()
        }
    }

    private fun signUp(){
        val email = binding.edtTxtEmail.text.toString()
        val password = binding.edtTxtPassword.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()){
            viewModel.signUp(User(email, password))
        }
    }

    private fun signIn(){
        val email = binding.edtTxtEmail.text.toString()
        val password = binding.edtTxtPassword.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()){
            viewModel.signIn(User(email, password))
        }

    }

    private fun observeSigningUp(){
        viewModel._signedUpUser.observe(viewLifecycleOwner, {
            if (it.status == ResultControl.Status.SUCCESS) {
                Toast.makeText(requireContext(), "Successfully signed up", Toast.LENGTH_SHORT)
                    .show()
            } else if (it.status == ResultControl.Status.ERROR) {
                Toast.makeText(requireContext(), "${it.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun observeSigningIn(){
        viewModel._signedInUser.observe(viewLifecycleOwner, {
            if (it.status == ResultControl.Status.SUCCESS) {
                Toast.makeText(requireContext(), "Successfully signed in", Toast.LENGTH_SHORT)
                    .show()
            } else if (it.status == ResultControl.Status.ERROR) {
                Toast.makeText(requireContext(), "${it.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }


}