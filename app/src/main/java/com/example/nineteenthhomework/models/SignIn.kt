package com.example.nineteenthhomework.models

data class SignIn(val idToken: String?,
                  val email: String?,
                  val refreshToken: String?,
                  val expiresIn: String?,
                  val localId: String?,
                  val registered: Boolean?
)
