package com.example.nineteenthhomework.models

data class SignUp(
    val idToken: String?,
    val email: String?,
    val refreshToken: String?,
    val expiresIn: String?,
    val localId: String?
)
