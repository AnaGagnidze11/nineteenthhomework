package com.example.nineteenthhomework.models

data class User(val email: String, val password: String, val returnSecureToken: Boolean = true)