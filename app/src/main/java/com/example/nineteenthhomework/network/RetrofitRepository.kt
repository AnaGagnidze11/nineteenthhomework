package com.example.nineteenthhomework.network

import com.example.nineteenthhomework.App
import com.example.nineteenthhomework.models.SignIn
import com.example.nineteenthhomework.models.SignUp
import com.example.nineteenthhomework.models.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface RetrofitRepository {
    @POST("v1/accounts:signUp?")
    suspend fun signUp(@Query("key") API_KEY: String = App.apiKey, @Body newUser: User): Response<SignUp>

    @POST("v1/accounts:signInWithPassword?")
    suspend fun signIn(@Query("key") API_KEY: String = App.apiKey, @Body currentUser: User): Response<SignIn>
}