package com.example.nineteenthhomework.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nineteenthhomework.App
import com.example.nineteenthhomework.models.SignIn
import com.example.nineteenthhomework.models.SignUp
import com.example.nineteenthhomework.models.User
import com.example.nineteenthhomework.network.ResultControl
import com.example.nineteenthhomework.network.RetrofitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SignUpViewModel: ViewModel() {

    private val signedUpUser = MutableLiveData<ResultControl<SignUp>>().apply {
        mutableListOf<SignUp>()
    }

    var _signedUpUser: LiveData<ResultControl<SignUp>> = signedUpUser

    private val signedInUser = MutableLiveData<ResultControl<SignIn>>().apply {
        mutableListOf<SignIn>()
    }

    var _signedInUser: LiveData<ResultControl<SignIn>> = signedInUser

    fun signUp(user: User){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                signUpUser(user)
            }
        }
    }

    fun signIn(user:User){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                signInUser(user)
            }
        }
    }



    private suspend fun signUpUser(user: User){
        signedUpUser.postValue(ResultControl.loading(true))
        val result = RetrofitService.retrofitService.signUp(App.apiKey, user)
        if (result.isSuccessful){
            val response = result.body()
            response?.let {
                signedUpUser.postValue(ResultControl.success(response))
            }
        }else{
            signedUpUser.postValue(ResultControl.error(result.errorBody().toString()))
        }
    }

    private suspend fun signInUser(user:User){
        signedInUser.postValue(ResultControl.loading(true))
        val result = RetrofitService.retrofitService.signIn(App.apiKey, user)
        if (result.isSuccessful){
            val response = result.body()
            response?.let {
                signedInUser.postValue(ResultControl.success(response))
            }
        }else{
            signedInUser.postValue(ResultControl.error(result.errorBody().toString()))
        }
    }
}